﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BankingAPI.Data.Models.Transaction;

namespace BankingAPI.Data.ViewModels
{
    public class MakeTransactionRequestViewModel
    {
        public Guid AccountId { get; set; }
        public double Amount { get; set; }
        public TransactionTypes TransactionType { get; set; }
    }
}
