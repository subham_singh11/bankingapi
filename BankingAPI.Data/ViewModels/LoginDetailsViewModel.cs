﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingAPI.Data.ViewModels
{
    public class LoginDetailsViewModel
    {
        public string Token { get; set; }
        public string ResponseMessage { get; set; }
    }
}
