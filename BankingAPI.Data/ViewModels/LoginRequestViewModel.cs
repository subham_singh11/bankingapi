﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingAPI.Data.ViewModels
{
    public class LoginRequestViewModel
    {
        public string CustomerEmail { get; set; }
        public string CustomerPassword { get; set; }
    }
}
