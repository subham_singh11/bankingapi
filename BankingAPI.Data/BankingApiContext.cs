﻿using BankingAPI.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingAPI.Data
{
    public class BankingApiContext : DbContext
    {
        public BankingApiContext(DbContextOptions<BankingApiContext> options) : base(options)
        {

        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Customer>()
                    .Property(c => c.CreatedAtTs)
                    .HasDefaultValueSql("GETDATE()");

            modelBuilder.Entity<Account>()
                .Property(a => a.CreatedAtTs)
                .HasDefaultValueSql("GETDATE()");

            modelBuilder.Entity<Transaction>()
                .Property(t => t.CreatedAtTs)
                .HasDefaultValueSql("GETDATE()");

            modelBuilder.Entity<Customer>()
                        .Property(c => c.UpdatedAtTs).HasDefaultValueSql("GETDATE()")
                        .ValueGeneratedOnUpdate();
        }
    }
}
