﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BankingAPI.Data.Models
{
    // Account Table
    public class Account
    {
        [Key]
        public Guid AccountId { get; set; }
        [Required]
        public double AmountDeposited { get; set; }
        [Required]
        public string AccountType { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime CreatedAtTs { get; set; }
        public DateTime? UpdatedAtTs { get; set; }

        [Required]
        [ForeignKey("Customer")]
        public Guid CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

    }
}
