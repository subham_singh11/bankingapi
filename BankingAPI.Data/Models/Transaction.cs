﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingAPI.Data.Models
{
    // Transaction Table
    public class Transaction
    {
        public enum TransactionTypes
        {
            deposit, withdraw
        }

        [Key]
        [Required]
        public Guid TransactionId { get; set; }
        public TransactionTypes TransactionType { get; set; }
        //public string TransactionType  { get; set; }
        [Required]
        public double PreviousBalance { get; set; }
        [Required]
        public double TransactionAmount { get; set; }
        [Required]
        public string TransactionStatus { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime CreatedAtTs { get; set; }

        // TODO : Tried to have dependency from Customer table but was getting error as "
        /*
         Introducing FOREIGN KEY constraint 'FK_Transactions_Customers_CustomerId' on table 'Transactions' may cause cycles or multiple cascade paths. Specify ON DELETE NO ACTION or ON UPDATE NO ACTION, or modify other FOREIGN KEY constraints. Could not create constraint or index. See previous errors.
         */
        // public Guid CustomerId { get; set; }
        // public Customer Customer { get; set; }
        public Guid AccountId { get; set; }
        public Account Account { get; set; }
    }
}
