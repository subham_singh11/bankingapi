﻿using BankingAPI.Controllers;
using BankingAPI.Data.Models;
using BankingAPI.Repository.Common;
using FakeItEasy;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace BankingApi.UnitTest
{
    public class TransactionControllerUnitTests
    {
        [Fact]
        public void GetAllTransactionsByIdShouldReturnListOfAllTransactionForThatAccountID()
        {
            var fakeCommonService = A.Fake<ICommonRepository>();
            var fakeAccountService = A.Fake<IAccountRepository>();
            var fakeAccount = A.Fake<Account>();

            A.CallTo(() => fakeCommonService.GetAllTransactionByAccountId(fakeAccount.AccountId)).Returns(A.CollectionOfFake<Transaction>(1));

            var transactionController = new TransactionController(fakeAccountService, fakeCommonService);
            var result = transactionController.GetAllTransactionsById(fakeAccount.AccountId);
            var okResult = result as OkObjectResult;
            Assert.Equal(200, okResult.StatusCode);
            Assert.IsAssignableFrom<IActionResult>(result);
        }
        
        /**
        [Fact]
        public void GetAllTransactionsByRandomIdShouldReturnNoContent()
        {
            var fakeCommonService = A.Fake<ICommonRepository>();
            var fakeAccountService = A.Fake<IAccountRepository>();
            var fakeAccount = A.Fake<Account>();

            A.CallTo(() => fakeCommonService.GetAllTransactionByAccountId(fakeAccount.AccountId)).Returns(A.CollectionOfFake<Transaction>(1));

            var transactionController = new TransactionController(fakeAccountService, fakeCommonService);
            var result = transactionController.GetAllTransactionsById(Guid.NewGuid());
            var noContentResult = result as NoContentResult;
            Assert.Equal(204, noContentResult.StatusCode);
            Assert.IsAssignableFrom<IActionResult>(result);
        }
        **/
    }
}
