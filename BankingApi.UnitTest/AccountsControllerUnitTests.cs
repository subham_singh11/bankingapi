using BankingAPI.Controllers;
using BankingAPI.Data;
using BankingAPI.Data.Models;
using BankingAPI.Repository.Common;
using FakeItEasy;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace BankingApi.UnitTest
{
    public class AccountsControllerUnitTests
    {
        /**
         * TODO: 
         * Get account details with correct ID
         * Get account details with incorrect ID <<---
         * **/
        [Fact]
        public void GetAccountByIdShouldReturnAccountData()
        {
            var fakeAccountService = A.Fake<IAccountRepository>();
            var fakeAccount = A.Fake<Account>();

            A.CallTo(() => fakeAccountService.GetAccount(A<Guid>.Ignored))
                .Returns(A.Fake<Account>());

            var accountController = new AccountsController(null, fakeAccountService);

            // Act
            var result = accountController.GetAccount(fakeAccount.AccountId);

            // Assert
            var okResult = result as OkObjectResult;

            Assert.Equal(200, okResult.StatusCode);
            Assert.IsAssignableFrom<IActionResult>(result);
        }
        
        /**
         * TODO:
         * Create new Account with Customer details
         * --> Pass on correct payload
         * --> Pass null payload
         * **************************************************************
         * --> pass customer ID of created customer in order to test the FK of account Table with Customer
         * --> Give Non existing customer ID for creating account
         * **/

        [Fact]
        public void CreateAccountShouldReturnNewAccount()
        {
            var fakeAccountService = A.Fake<IAccountRepository>();
            var fakeAccount = A.Fake<Account>();

            A.CallTo(() => fakeAccountService.CreateAccount(A.Fake<Account>())).Returns(A.Fake<Account>());
            var accountController = new AccountsController(null, fakeAccountService);

            var result = accountController.PostAccount(fakeAccount);

            var createdAtActionResult = result as CreatedAtActionResult;

            Assert.Equal(201, createdAtActionResult.StatusCode);
            Assert.IsAssignableFrom<IActionResult>(result);
        }

        [Fact]
        public void CreateAccountWithNullShouldReturnBadRequest()
        {
            var fakeAccountService = A.Fake<IAccountRepository>();
            // var fakeAccount = A.Fake<Account>();

           // A.CallTo(() => fakeAccountService.CreateAccount(A.Fake<Account>())).Returns(A.Fake<Account>());
            var accountController = new AccountsController(null, fakeAccountService);

            var result = accountController.PostAccount(null);

            var badRequestResult = result as BadRequestResult;

            Assert.Equal(400, badRequestResult.StatusCode);
            Assert.IsAssignableFrom<IActionResult>(result);
        }


        /**
         * TODO:
         * Get All Accounts
         * Get multiple Accounts details as list <<--- 
         * **/

        [Fact]
        public void GetAccountsShouldReturnAccountList()
        {
            var fakeAccountService = A.Fake<IAccountRepository>();

            A.CallTo(() => fakeAccountService.GetAccounts()).Returns(A.CollectionOfFake<Account>(1));
            var accountController = new AccountsController(null, fakeAccountService);
            var result = accountController.GetAccounts();
            var okResult = result as OkObjectResult;

            Assert.Equal(200, okResult.StatusCode);
            Assert.IsAssignableFrom<IActionResult>(result);
        }
    }
}
