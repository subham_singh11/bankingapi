﻿using BankingAPI.Controllers;
using BankingAPI.Data.Models;
using BankingAPI.Repository.Common.Impl.CustomerRepository;
using FakeItEasy;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BankingApi.UnitTest
{
    public class CustomerControllerUnitTests
    {
        /**
         * TODO:
         * Get All Customers
         * **/
        [Fact]
        public void GetCustomersShouldReturnCustomerList()
        {
            var fakeCustomerService = A.Fake<ICustomerRepository>();

            A.CallTo(() => fakeCustomerService.GetCustomers()).Returns(A.CollectionOfFake<Customer>(1));

            var customerController = new CustomersController(null, fakeCustomerService);
            var result = customerController.GetCustomers();
            var okResult = result as OkObjectResult;
            Assert.Equal(200, okResult.StatusCode);
            Assert.IsAssignableFrom<IActionResult>(result);
        }

        /**
         * TODO:
         * Get customer details with correct id
         * Get customer details with incorrect id 
         * **/

        [Fact]
        public void GetCustomerByIdShouldReturnCustomerData()
        {
            var fakeCustomerService = A.Fake<ICustomerRepository>();
            var fakeCustomer = A.Fake<Customer>();
            A.CallTo(() => fakeCustomerService.GetCustomer(fakeCustomer.CustomerId)).Returns(fakeCustomer);

            var customerController = new CustomersController(null, fakeCustomerService);
            var result = customerController.GetCustomer(fakeCustomer.CustomerId);

            var okResult = result as OkObjectResult;
            Assert.Equal(200, okResult.StatusCode);
            Assert.IsAssignableFrom<IActionResult>(result);
        }

        [Fact]
        public void CreateAndUpdateCustomerShouldReturnCustomerData()
        {
            var fakeCustomerService = A.Fake<ICustomerRepository>();
            var fakeCustomer = A.Fake<Customer>();
            // First create customer
            // Then Update customer
            A.CallTo(() => fakeCustomerService.CreateCustomer(fakeCustomer)).Returns(fakeCustomer);
            // Just for testing I have updated the CustomerEmail in fake Object

            var customerController = new CustomersController(null, fakeCustomerService);
            var newlyCreated = customerController.PostCustomer(fakeCustomer);
            var createdResult = newlyCreated as CreatedAtActionResult;
            Assert.Equal(201, createdResult.StatusCode);
            Assert.IsAssignableFrom<IActionResult>(createdResult);

            fakeCustomer.CustomerEmail = "test@email.com";
            A.CallTo(() => fakeCustomerService.UpdateCustomerProfile(fakeCustomer.CustomerId, fakeCustomer)).Returns(fakeCustomer);

            var updatedCustomer = customerController.PutCustomer(fakeCustomer.CustomerId, fakeCustomer);

            var updatedResult = updatedCustomer as OkObjectResult;
            Assert.Equal(200, updatedResult.StatusCode);
            Assert.IsAssignableFrom<IActionResult>(updatedResult);
        }

        [Fact]
        public void CreateCustomerWithNullDataShouldReturnBadRequest()
        {
            var fakeCustomerService = A.Fake<ICustomerRepository>();
            
            var customerController = new CustomersController(null, fakeCustomerService);
            var newlyCreated = customerController.PostCustomer(null);
            var badRequestResult = newlyCreated as BadRequestResult;
            Assert.Equal(400, badRequestResult.StatusCode);
            Assert.IsAssignableFrom<IActionResult>(badRequestResult);
        }

        [Fact]
        public void UpdateCustomerWithUnequalCustomerIdInRequestShouldReturnCustomerData()
        {
            var fakeCustomerService = A.Fake<ICustomerRepository>();
            var fakeCustomer = A.Fake<Customer>();

            var customerController = new CustomersController(null, fakeCustomerService);

            fakeCustomer.CustomerEmail = "test@email.com";

            var updatedCustomer = customerController.PutCustomer(Guid.NewGuid(), fakeCustomer);

            var badRequestResult = updatedCustomer as BadRequestResult;
            Assert.Equal(400, badRequestResult.StatusCode);
            Assert.IsAssignableFrom<IActionResult>(badRequestResult);
        }

        /**
        [Fact]
        public void CreateAndUpdateCustomerWithRandomIdShouldReturnNotFound()
        {
            var fakeCustomerService = A.Fake<ICustomerRepository>();
            var fakeCustomer = A.Fake<Customer>();
            A.CallTo(() => fakeCustomerService.CreateCustomer(fakeCustomer)).Returns(fakeCustomer);

            var customerController = new CustomersController(null, fakeCustomerService);
            var newlyCreated = customerController.PostCustomer(fakeCustomer);
            var createdResult = newlyCreated as CreatedAtActionResult;
            Assert.Equal(201, createdResult.StatusCode);
            Assert.IsAssignableFrom<IActionResult>(createdResult);

            fakeCustomer.CustomerEmail = "test@email.com";
            A.CallTo(() => fakeCustomerService.UpdateCustomerProfile(fakeCustomer.CustomerId, fakeCustomer)).Returns(fakeCustomer);

            var updatedCustomer = customerController.PutCustomer(Guid.NewGuid(), fakeCustomer);

            var notFoundResult = updatedCustomer as NotFoundResult;
            Assert.Equal(404, notFoundResult.StatusCode);
            Assert.IsAssignableFrom<IActionResult>(notFoundResult);
        }

        **/


    }
}
