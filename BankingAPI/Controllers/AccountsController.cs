﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BankingAPI.Data;
using BankingAPI.Data.Models;
using BankingAPI.Repository.Common;
using Microsoft.AspNetCore.Authorization;

namespace BankingAPI.Controllers
{
    // [Authorize]
    [Route("api/banking/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly BankingApiContext _context;
        private IAccountRepository _accountRepository;

        public AccountsController(BankingApiContext context, IAccountRepository accountRepository)
        {
            _context = context;
            _accountRepository = accountRepository;
        }

        // GET: api/banking/Accounts
        [Authorize]
        [HttpGet]
        public IActionResult GetAccounts()
        {
            var accountData = _accountRepository.GetAccounts().Result;
            return Ok(accountData);
        }

        // GET: api/banking/Accounts/5
        [Authorize]
        [HttpGet("{id}")]
        public IActionResult GetAccount(Guid id)
        {
            Account accountData = _accountRepository.GetAccount(id).Result;

            if (accountData == null)
            {
                return NotFound();
            }

            return Ok(accountData);
        }

        // POST: api/banking/Accounts
        [HttpPost]
        public IActionResult PostAccount(Account account)
        {
            if (account == null)
            {
                return BadRequest();
            }
            else
            {
                var newAccount = _accountRepository.CreateAccount(account).Result;
                if (newAccount == null)
                {
                    return NotFound("Customer with id " + account.CustomerId + " not found");
                }
                return CreatedAtAction(
                    "GetAccount",
                    new { id = account.AccountId },
                    account);
            }
        }

        /**
        // DELETE: api/banking/Accounts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAccount(Guid id)
        {
            var account = await _context.Accounts.FindAsync(id);
            if (account == null)
            {
                return NotFound();
            }

            _context.Accounts.Remove(account);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        **/

        /**
        private bool AccountExists(Guid id)
        {
            return _context.Accounts.Any(e => e.AccountId == id);
        }
        **/
    }
}
