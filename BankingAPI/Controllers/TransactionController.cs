﻿using BankingAPI.Data.Models;
using BankingAPI.Repository.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankingAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {

        private ICommonRepository _commonRepository;
        private readonly IAccountRepository _accountRepository;


        public TransactionController(IAccountRepository accountRepository, ICommonRepository commonRepository)
        {
            _commonRepository = commonRepository;
            _accountRepository = accountRepository;
        }

        [HttpGet("{accountId}")]
        public IActionResult GetAllTransactionsById(Guid accountId)
        {
            Account existingAccount = _accountRepository.GetAccount(accountId).Result;
            if(existingAccount != null)
            {
                var transactionData = _commonRepository.GetAllTransactionByAccountId(accountId).Result;
                return Ok(transactionData);
            }
            else
            // TODO: Add meaning full Object in for NOContent()
            {
                return NoContent();
            }
            
        }

    }
}
