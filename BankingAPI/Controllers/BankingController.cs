﻿using BankingAPI.Data.Models;
using BankingAPI.Data.ViewModels;
using BankingAPI.Repository.Common;
using BankingAPI.Repository.Common.Impl;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace BankingAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class BankingController : ControllerBase
    {
        private IAccountRepository _accountRepository;
        private readonly ITransactionRepository _transactionRepository;
        private ICommonRepository _commonRepository;
        public BankingController(IAccountRepository accountRepository, ITransactionRepository transactionRepository, ICommonRepository commonRepository)
        {
            _accountRepository = accountRepository;
            _transactionRepository = transactionRepository;
            _commonRepository = commonRepository;
        }

        // TODO : Add View Model for account and Customer Details
        [HttpGet("{accountId}")]
        public async Task<ActionResult<CustomerDetailsViewModel>> GetAccountWithCustomerDetail(Guid accountId)
        {
            CustomerDetailsViewModel finalData = await _commonRepository.GetAccountWithCustomerDetail(accountId);

            if (finalData == null)
            {
                return NotFound();
            }

            return finalData;
        }

        /**
        [HttpPost]
        [Route("deposit/")]
        public async Task<ActionResult<Transaction>> DepositTransaction(Guid accountId, double amount)
        {
            string transactionType = "deposit";

            var accountData = await _accountRepository.GetAccount(accountId);

            if (accountData == null)
            {
                return NotFound("Account with id " + accountId + " not available.");
            }
            else
            {
                var transactionData = await _commonRepository.DoTransactionWithAccountId(accountId, amount, transactionType);
                if (transactionData != null)
                {
                    // TODO : Add status as created => amountChanged ==> transactionData
                    return Ok(transactionData);
                }
                else
                {
                    // TODO: Add some meaningful message for the transaction history not updaed
                    return Ok("Something went wrong with transaction");
                }
            }
        }


        [HttpPost]
        [Route("withdraw/")]
        public async Task<ActionResult<Transaction>> WithdrawTransaction(Guid accountId, double amount)
        {

            string transactionType = "withdraw";

            var accountData = await _accountRepository.GetAccount(accountId);

            if (accountData == null)
            {
                return NotFound("Account with id " + accountId + " not available.");
            }
            else
            {
                var transactionData = await _commonRepository.DoTransactionWithAccountId(accountId, amount, transactionType);
                if (transactionData != null)
                {
                    // TODO : Add status as created => amountChanged ==> transactionData
                    return Ok(transactionData);
                }
                else
                {
                    // TODO: Add some meaningful message for the transaction history not updaed
                    return Ok("Something went wrong with transaction");
                }
            }
        }
        **/

        [HttpPost]
        [Route("transaction/")]
        public async Task<ActionResult<Transaction>> DoTransaction(MakeTransactionRequestViewModel makeTransactionRequestView)
        {

            // string transactionType = "withdraw";

            var accountData = await _accountRepository.GetAccount(makeTransactionRequestView.AccountId);

            if (accountData == null)
            {
                return NotFound("Account with id " + makeTransactionRequestView.AccountId + " not available.");
            }
            else
            {
                var transactionData = await _commonRepository.DoTransactionWithAccountId(makeTransactionRequestView.AccountId, makeTransactionRequestView.Amount, makeTransactionRequestView.TransactionType);
                if (transactionData != null)
                {
                    // TODO : Add status as created => amountChanged ==> transactionData
                    return Ok(transactionData);
                }
                else
                {
                    // TODO: Add some meaningful message for the transaction history not updaed
                    return Ok("Something went wrong with transaction");
                }
            }
        }
    }
}
