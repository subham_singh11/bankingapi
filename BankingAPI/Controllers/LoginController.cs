﻿using BankingAPI.Data;
using BankingAPI.Data.Models;
using BankingAPI.Data.ViewModels;
using BankingAPI.Repository.Common.Impl.LoginRepository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace BankingAPI.Controllers
{
    [Route("api/banking/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        public IConfiguration _configuration;
        private readonly BankingApiContext _context;
        private readonly ILoginRepository _loginRepository;

        public LoginController(IConfiguration configuration, BankingApiContext context, ILoginRepository loginRepository)
        {
            _configuration = configuration;
            _context = context;
            _loginRepository = loginRepository;
        }

        [HttpPost]
        public async Task<ActionResult<LoginDetailsViewModel>> Login(LoginRequestViewModel loginRequestViewModel)
        {
            if(loginRequestViewModel.CustomerEmail != null && loginRequestViewModel.CustomerPassword != null)
            {
                var token = await _loginRepository.LoginCustomer(loginRequestViewModel.CustomerEmail, loginRequestViewModel.CustomerPassword);
                LoginDetailsViewModel loginDetailsViewModel = new LoginDetailsViewModel();
                if (token != null)
                {
                    loginDetailsViewModel.Token = token;
                    loginDetailsViewModel.ResponseMessage = "Valid Credential";
                    return Ok(loginDetailsViewModel);
                }
                else
                {
                    loginDetailsViewModel.Token = null;
                    loginDetailsViewModel.ResponseMessage = "Invalid Credential";
                    return Unauthorized(loginDetailsViewModel);
                }
            } 
            else
            {

                return BadRequest();
            }
        }
    }
}
