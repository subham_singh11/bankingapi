﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BankingAPI.Data;
using BankingAPI.Data.Models;
using BankingAPI.Repository.Common.Impl.CustomerRepository;
using Microsoft.AspNetCore.Authorization;

namespace BankingAPI.Controllers
{
    [Route("api/banking/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly BankingApiContext _context;
        private readonly ICustomerRepository _customerRepository;

        public CustomersController(BankingApiContext context, ICustomerRepository customerRepository)
        {
            _context = context;
            _customerRepository = customerRepository;
        }

        // GET: api/banking/Customers
        // Get All Customers
        // [Authorize]
        [HttpGet]
        public IActionResult GetCustomers()
        {
            var customerList = _customerRepository.GetCustomers().Result;
            return Ok(customerList);
        }


        // GET: api/banking/Customers/5
        // Get Customer Profile API by ID
        [Authorize]
        [HttpGet("{id}")]
        public IActionResult GetCustomer(Guid id)
        {
            // var customer = await _context.Customers.FindAsync(id);
            var customer = _customerRepository.GetCustomer(id).Result;

            if (customer == null)
            {
                return NotFound();
            }

            return Ok(customer);
        }

        // PUT: api/banking/Customers/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [Authorize]
        [HttpPut("{id}")]
        public IActionResult PutCustomer(Guid id, Customer customer)
        {
            if (id != customer.CustomerId)
            {
                return BadRequest();
            }

            // _context.Entry(customer).State = EntityState.Modified;

            try
            {
                // await _context.SaveChangesAsync();
                // TODO: It is not working needed to be passed from frontend
                customer.UpdatedAtTs = DateTime.UtcNow;
                var updatedCustomerData = _customerRepository.UpdateCustomerProfile(id, customer).Result;
                return Ok(updatedCustomerData);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_customerRepository.IsCustomerAddedToSystem(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            //return NoContent();
        }

        
        // POST: api/banking/Customers
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        // Register Customer in the System can be done while creating account in the bank
        [HttpPost]
        public IActionResult PostCustomer(Customer customer)
        {
            // _context.Customers.Add(customer);
            // await _context.SaveChangesAsync();
            if(customer == null)
            {
                return BadRequest();
            } else
            {
                Customer customerData = _customerRepository.CreateCustomer(customer).Result;
                return CreatedAtAction("GetCustomer", new { id = customerData.CustomerId }, customerData);
            }
        }
        
        /**
        // DELETE: api/banking/Customers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _context.Customers.FindAsync(id);
            if (customer == null)
            {
                return NotFound();
            }

            _context.Customers.Remove(customer);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        **/
    }
}
