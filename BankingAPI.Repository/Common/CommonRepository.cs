﻿using BankingAPI.Data;
using BankingAPI.Data.Models;
using BankingAPI.Data.ViewModels;
using BankingAPI.Repository.Common.Impl;
using BankingAPI.Repository.Common.Impl.CustomerRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BankingAPI.Data.Models.Transaction;

namespace BankingAPI.Repository.Common
{
    public class CommonRepository : ICommonRepository
    {
        private BankingApiContext _bankingApiContext;
        private IAccountRepository _accountRepository;
        private ICustomerRepository _customerRepository;
        private ITransactionRepository _transactionRepository;

        public CommonRepository(BankingApiContext bankingApiContext, IAccountRepository accountRepository, ICustomerRepository customerRepository, ITransactionRepository transactionRepository)
        {
            _bankingApiContext = bankingApiContext;
            _accountRepository = accountRepository;
            _customerRepository = customerRepository;
            _transactionRepository = transactionRepository;
        }

        public Task<Transaction> DepositedMoney(Guid customerId, Guid accountId, double amount)
        {
            throw new NotImplementedException();
        }

        public async Task<Transaction> DoTransactionWithAccountId(Guid accountId, double amount, TransactionTypes transactionType)
        {
            try
            {
                var accountDetail = await _accountRepository.GetAccount(accountId);

                Transaction newTransaction = new Transaction
                {
                    TransactionId = Guid.NewGuid(),
                    TransactionAmount = amount,
                    TransactionType = transactionType,
                    PreviousBalance = accountDetail.AmountDeposited,
                    CreatedAtTs = DateTime.UtcNow
                };

                if (transactionType == TransactionTypes.deposit)
                {
                    var depositedAmount = accountDetail.AmountDeposited + amount;
                    var updatedAccount = await _accountRepository.UpdateAccountAmount(accountId, amount, transactionType);

                    if (updatedAccount.AmountDeposited == depositedAmount && newTransaction != null)
                    {
                        newTransaction.TransactionStatus = "success";
                        newTransaction.AccountId = accountId;
                        Transaction newlyAddedTransaction = await _transactionRepository.AddTransactionHistory(newTransaction);
                        return newlyAddedTransaction;
                    }
                    else
                    {
                        newTransaction.TransactionStatus = "failure";
                        newTransaction.AccountId = accountId;
                        Transaction newlyAddedTransaction = await _transactionRepository.AddTransactionHistory(newTransaction);
                        return newlyAddedTransaction;
                    }
                }
                else if (transactionType == TransactionTypes.withdraw)
                {
                    double withdrawAmount;
                    if (accountDetail.AmountDeposited >= 0)
                    {
                        withdrawAmount = accountDetail.AmountDeposited - amount;

                        var updatedAccount = await _accountRepository.UpdateAccountAmount(accountId, amount, transactionType);

                        if (updatedAccount.AmountDeposited == withdrawAmount && newTransaction != null)
                        {
                            newTransaction.TransactionStatus = "success";
                            newTransaction.AccountId = accountId;
                            Transaction newlyAddedTransaction = await _transactionRepository.AddTransactionHistory(newTransaction);
                            return newlyAddedTransaction;
                        }
                        else
                        {
                            newTransaction.TransactionStatus = "failure";
                            newTransaction.AccountId = accountId;
                            Transaction newlyAddedTransaction = await _transactionRepository.AddTransactionHistory(newTransaction);
                            return newlyAddedTransaction;
                        }
                    } else
                    {
                        // return amount too low
                        return null;
                    }
                }
                else
                {
                    // TODO : Bad implementation need to add view model
                    return null;
                }
            }
            catch (Exception)
            {
                // TODO : Bad implementation need to add view model
                return null;
            }
        }

        public async Task<CustomerDetailsViewModel> GetAccountWithCustomerDetail(Guid accountId)
        {
            Account accountData = await _accountRepository.GetAccount(accountId);
            var customerId = accountData.CustomerId;

            // Customer customerData = await _customerRepository.GetCustomer(accountData.CustomerId);

            CustomerDetailsViewModel customerDetailsViewModel = new CustomerDetailsViewModel()
            {
                Account = accountData,
                Customer = await _customerRepository.GetCustomer(customerId),
                ResponseMessage = "Customer with Accounts Data Fetched Successfully"
            };

            // return overall data of customer With Account 
            // dynamic data = new ExpandoObject();
            // data = accountData;
            // data.Account = accountData;
            // data.Customer = customerData;
            // return data;

            return customerDetailsViewModel;
        }

        public async Task<object> GetAllTransactionByAccountId(Guid accountId)
        {
            IQueryable<object> transactionData = from ele in _bankingApiContext.Transactions
                                                 where ele.AccountId == accountId
                                                 select ele;
            return await transactionData.ToListAsync();
        }
    }
}
