﻿using BankingAPI.Data;
using BankingAPI.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingAPI.Repository.Common.Impl
{
    public class TransactionRepository : ITransactionRepository
    {
        private BankingApiContext _bankingApiContext;
        public TransactionRepository(BankingApiContext bankingApiContext)
        {
            _bankingApiContext = bankingApiContext;
        }
        public async Task<Transaction> AddTransactionHistory(Transaction transaction)
        {
            if(transaction != null)
            {
                _bankingApiContext.Transactions.Add(transaction);
                await _bankingApiContext.SaveChangesAsync();
                Task<Transaction> newlyTransactionLog = GetTransaction(transaction.TransactionId);
                return await newlyTransactionLog;
            } else
            {
                return null;
            }
            throw new NotImplementedException();
        }

        public async Task<Transaction> GetTransaction(Guid transactionId)
        {
            return await _bankingApiContext.Transactions.FindAsync(transactionId);
        }
    }
}
