﻿using BankingAPI.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingAPI.Repository.Common.Impl
{
    public interface ITransactionRepository
    {
        public Task<Transaction> GetTransaction(Guid transactionId);
        public Task<Transaction> AddTransactionHistory(Transaction transaction);
    }
}
