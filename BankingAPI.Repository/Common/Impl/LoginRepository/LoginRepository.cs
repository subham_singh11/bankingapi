﻿using BankingAPI.Data;
using BankingAPI.Repository.Common.Impl.CustomerRepository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BankingAPI.Repository.Common.Impl.LoginRepository
{
    public class LoginRepository : ILoginRepository
    {
        // private readonly BankingApiContext _bankingApiContext;
        public IConfiguration _configuration;
        private readonly ICustomerRepository _customerRepository;
        public LoginRepository(BankingApiContext bankingApiContext, ICustomerRepository customerRepository, IConfiguration configuration)
        {
            // _bankingApiContext = bankingApiContext;
            _customerRepository = customerRepository;
            _configuration = configuration;
        }
        public async  Task<string> LoginCustomer(string customerEmailId, string customerPassword)
        {
            var existingUser = await _customerRepository.GetUserByEmailAndPassword(customerEmailId, customerPassword);

            if (existingUser != null)
            {

                var claims = new[] {
                    new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                    new Claim("CustomerId", existingUser.CustomerId.ToString()),
                    new Claim("CustomerEmail", existingUser.CustomerEmail.ToString()),
                    new Claim("CustomerName", existingUser.CustomerName.ToString())
                };
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));

                var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                JwtSecurityToken token = new JwtSecurityToken(_configuration["Jwt:Issuer"], _configuration["Jwt:Audience"], claims, expires: DateTime.UtcNow.AddDays(1), signingCredentials: signIn);

                string stringToken = new JwtSecurityTokenHandler().WriteToken(token);

                return stringToken;
            }
            else
            {
                return null;
            }
        }
    }
}
