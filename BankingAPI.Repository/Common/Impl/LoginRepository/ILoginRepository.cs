﻿using BankingAPI.Data.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingAPI.Repository.Common.Impl.LoginRepository
{
    public interface ILoginRepository
    {
        public Task<string> LoginCustomer(string customerEmailId, string customerPassword);
    }
}
