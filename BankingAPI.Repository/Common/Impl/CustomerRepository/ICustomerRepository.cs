﻿using BankingAPI.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingAPI.Repository.Common.Impl.CustomerRepository
{
    public interface ICustomerRepository
    {
        public Task<Customer> CreateCustomer(Customer customer);
        public bool IsCustomerAddedToSystem(Guid customerId);
        public Task<IEnumerable<Customer>> GetCustomers();
        public Task<Customer> GetCustomer(Guid customerId);
        public Task<Customer> UpdateCustomerProfile(Guid customerId, Customer customer);

        public Task<Customer> GetUserByEmailAndPassword(string email, string password);
    }
}

/*
Withdraw amount
deposit amount
login api
jwt authentication implementattion
update Customer profile
 */