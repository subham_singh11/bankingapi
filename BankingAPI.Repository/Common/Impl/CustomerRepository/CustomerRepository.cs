﻿using BankingAPI.Data;
using BankingAPI.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingAPI.Repository.Common.Impl.CustomerRepository
{
    public class CustomerRepository : ICustomerRepository
    {
        private BankingApiContext _bankingApiContext;
        public CustomerRepository(BankingApiContext bankingApiContext)
        {
            _bankingApiContext = bankingApiContext;
        }

        public async Task<Customer> CreateCustomer(Customer customer)
        {
            _bankingApiContext.Customers.Add(customer);
            await _bankingApiContext.SaveChangesAsync();
            // TODO : Use of LastAsync
            var customerData = await _bankingApiContext.Customers.FirstOrDefaultAsync(ele => ele.CustomerEmail == customer.CustomerEmail);
            return customerData;

        }

        public async Task<Customer> GetCustomer(Guid customerId)
        {
            return await _bankingApiContext.Customers.FindAsync(customerId);
        }

        public async Task<IEnumerable<Customer>> GetCustomers()
        {
            var customers_data = from ele in _bankingApiContext.Customers
                               select ele;
            return await customers_data.ToListAsync();
        }

        public async Task<Customer> GetUserByEmailAndPassword(string email, string password)
        {
            return await _bankingApiContext.Customers.FirstOrDefaultAsync(ele => ele.CustomerEmail == email && ele.CustomerPassword == password);
        }

        public bool IsCustomerAddedToSystem(Guid customerId)
        {
            bool isCustomerAvailable = _bankingApiContext.Customers.Any(ele => ele.CustomerId == customerId);
            return isCustomerAvailable;
        }

        public async Task<Customer> UpdateCustomerProfile(Guid customerId, Customer customer)
        {
            _bankingApiContext.Entry(customer).State = EntityState.Modified;
            await _bankingApiContext.SaveChangesAsync();
            Task<Customer> existingCustomer = _bankingApiContext.Customers.FirstOrDefaultAsync(ele => ele.CustomerId == customerId);
            return await existingCustomer;
        }
    }
}
