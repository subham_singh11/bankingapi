﻿using BankingAPI.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BankingAPI.Data.Models.Transaction;

namespace BankingAPI.Repository.Common
{
    public interface IAccountRepository
    {
        public Task<IEnumerable<Account>> GetAccounts();
        public Task<Account> GetAccount(Guid id);
        public bool IsAccountExist(Guid accountId);
        public Task<Account> CreateAccount(Account accountPayLoad);
        public Task<Account> UpdateAccountAmount(Guid accountId, double amount, TransactionTypes transactionType);
    }
}
