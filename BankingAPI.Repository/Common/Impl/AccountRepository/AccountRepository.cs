﻿using BankingAPI.Data;
using BankingAPI.Data.Models;
using BankingAPI.Repository.Common.Impl.CustomerRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static BankingAPI.Data.Models.Transaction;

namespace BankingAPI.Repository.Common.Impl
{
    public class AccountRepository : IAccountRepository
    {
        private BankingApiContext _bankingApiContext;
        private readonly ICustomerRepository _customerRepository;
        private readonly ITransactionRepository _transactionRepository;

        public AccountRepository(BankingApiContext bankingApiContext, ICustomerRepository customerRepository, ITransactionRepository transactionRepository)
        {
            _bankingApiContext = bankingApiContext;
            _customerRepository = customerRepository;
            _transactionRepository = transactionRepository;

        }

        public async Task<Account> CreateAccount(Account accountPayLoad)
        {
            bool isCustomerPresent = _bankingApiContext.Customers.Any(ele => ele.CustomerId == accountPayLoad.CustomerId);

            if (isCustomerPresent)
            {
                _bankingApiContext.Accounts.Add(accountPayLoad);
                await _bankingApiContext.SaveChangesAsync();
                var newlyCreatedAccount = await GetAccount(accountPayLoad.AccountId);
                return newlyCreatedAccount;
            }
            else
            {

                // If the new customer data not present
                if (accountPayLoad.Customer == null)
                {
                    return null;
                }
                else
                {
                    _bankingApiContext.Accounts.Add(accountPayLoad);
                    await _bankingApiContext.SaveChangesAsync();
                    var newlyCreatedAccount = await GetAccount(accountPayLoad.AccountId);
                    return newlyCreatedAccount;
                }
            }
        }

        public async Task<Account> GetAccount(Guid accountId)
        {
            var accountData = await _bankingApiContext.Accounts.FindAsync(accountId);
            return accountData;
        }

        // TODO : It is returing customer detail as null need to get customer id as FK and search for that specific customer 
        public async Task<IEnumerable<Account>> GetAccounts()
        {
            // return await _bankingApiContext.Accounts.ToListAsync();

            var account_data = from ele in _bankingApiContext.Accounts
                               select ele;
            return await account_data.ToListAsync();
        }

        public bool IsAccountExist(Guid accountId)
        {
            var isAccountAvailable = _bankingApiContext.Accounts.Any(ele => ele.AccountId == accountId);
            return isAccountAvailable;
        }

        public async Task<Account> UpdateAccountAmount(Guid accountId, double amount, TransactionTypes transactionType)
        {
            // TODO: Most of the time lastAsync works file so should I use it or not : Discuss
            // Account accountData = await _bankingApiContext.Accounts.LastAsync<Account>();
            Account accountData = await _bankingApiContext.Accounts.FirstOrDefaultAsync(ele => ele.AccountId == accountId);
            if (transactionType == TransactionTypes.deposit)
            {
                accountData.AmountDeposited += amount;
                accountData.UpdatedAtTs = DateTime.UtcNow;

            }
            else if (transactionType == TransactionTypes.withdraw)
            {
                accountData.AmountDeposited -= amount;
                accountData.UpdatedAtTs = DateTime.UtcNow;
            }
            else
            {
                accountData.AmountDeposited += 0;
                accountData.UpdatedAtTs = DateTime.UtcNow;
            }
            await _bankingApiContext.SaveChangesAsync();

            var updatedAccount = await GetAccount(accountId);
            return updatedAccount;
        }
    }
}
