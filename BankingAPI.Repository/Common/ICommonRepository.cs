﻿using BankingAPI.Data.Models;
using BankingAPI.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BankingAPI.Data.Models.Transaction;

namespace BankingAPI.Repository.Common
{
    public interface ICommonRepository
    {
        public Task<Transaction> DepositedMoney(Guid customerId, Guid accountId, double amount);
        public Task<Transaction> DoTransactionWithAccountId(Guid accountId, double amount, TransactionTypes transactionType);
        public Task<CustomerDetailsViewModel> GetAccountWithCustomerDetail(Guid accountId);
        public Task<Object> GetAllTransactionByAccountId(Guid accountId);

    }
}
